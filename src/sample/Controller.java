package sample;

import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

public class Controller {
    public Pane scene;
    public ImageView arrow;
    public Label infoLabel;
    public Label scoreLabel;
    public Label attemptsLabel;

    private Service service = new Service();

    public void initialize() {
        scene.setOnKeyPressed(event -> {
            switch (event.getCode()) {
                case ENTER:
                    confirm();
                    break;
                case SHIFT:
                    repeat();
                    break;
                default:
                    selectNote(event.getCode());
            }
        });
    }

    public void repeat() {
        infoLabel.setText("");
        service.repeat();
    }

    public void confirm() {
        String confirm = service.confirm();
        infoLabel.setText(confirm);
        if (confirm.contains("Correct")) {
            infoLabel.setTextFill(Color.web("#00FF00"));
        } else {
            infoLabel.setTextFill(Color.web("#FF0000"));
        }
        scoreLabel.setText("Score: " + service.getScore());
    }

    private void selectNote(KeyCode code) {
        service.selectNote(code);
        attemptsLabel.setText("Attempts: " + service.getTotalAttempts());
        infoLabel.setText("");
        setArrowPosition();
    }

    private void setArrowPosition() {
        int[] arr = service.setArrowPosition(service.getUserSelectedNote());
        arrow.setLayoutX(arr[0]);
        arrow.setLayoutY(arr[1]);
    }
}
