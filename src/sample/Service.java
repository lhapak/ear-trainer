package sample;

import javafx.scene.input.KeyCode;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

class Service {
    private List<String> keys = Arrays.asList("Z", "S", "X", "D", "C", "V", "G", "B", "H", "N", "J", "M",
            "Q", "DIGIT2", "W", "DIGIT3", "E", "R", "DIGIT5", "T", "DIGIT6", "Y", "DIGIT7", "U");
    private List<String> notes = Arrays.asList("C3", "C#3", "D3", "D#3", "E3", "F3", "F#3", "G3", "G#3",
            "A3", "A#3", "B3", "C4", "C#4", "D4", "D#4", "E4", "F4", "F#4", "G4", "G#4", "A4", "A#4", "B4");

    private List<String> filteredNotes = new ArrayList<>();
    private Map<String, String> keyNoteMap = new HashMap<>();

    private Random randomGenerator = new Random();
    private String userSelectedNote = "";
    private String drawnNote = "";
    private int totalAttempts = 0;
    private int score = 0;

    Service() {
        keyNoteMapping();
        filterNotes();
        getANote();
    }

    String getUserSelectedNote() {
        return userSelectedNote;
    }

    int getScore() {
        return score;
    }

    int getTotalAttempts() {
        return totalAttempts;
    }

    int[] setArrowPosition(String selectedNote) {
        int x = 50 + filteredNotes.indexOf(selectedNote.replace("#", "")) * 35;
        int y = 480;
        if (selectedNote.contains("#")) {
            x += 17;
            y -= 46;
        }
        return new int[]{x, y};
    }

    void selectNote(KeyCode code) {
        if (keyNoteMap.containsKey(code.toString())) {
            userSelectedNote = keyNoteMap.get(code.toString());
            totalAttempts++;
            playNote(userSelectedNote);
        }
    }

    void repeat() {
        playNote(drawnNote);
    }

    String confirm() {
        if (drawnNote.equals(userSelectedNote)) {
            getANote();
            score++;
            return "Correct: " + userSelectedNote;
        } else {
            return "Wrong: " + userSelectedNote;
        }
    }

    private void getANote() {
        drawnNote = notes.get(randomGenerator.nextInt(notes.size()));
        playNote(drawnNote);
    }

    private void playNote(String note) {
        Media m = new Media(Paths.get("src/audio/" + note + ".mp3").toUri().toString());
        new MediaPlayer(m).play();
    }

    private void filterNotes() {
        filteredNotes = notes.stream().filter(x -> !x.contains("#")).collect(Collectors.toList());
    }

    private void keyNoteMapping() {
        for (int i = 0; i < notes.size(); i++) {
            keyNoteMap.put(keys.get(i), notes.get(i));
        }
    }
}
